

/*For ACTS Index page*/

.course-title {
    font-size: 50%;
	font-weight:normal;
	text-shadow: 2px 2px #585858;
    text-align: center;
}

.custom-sep-right {
    border-right: 1px solid #fff;
    height:110px;
    margin-top:6px;
}
.statistic-part-second {
   	margin-bottom:5px;
	
}


.custom-course-title {
    background-color: #2e2d3f;
    color: #fff;
    padding: 7px 10px;
    margin-top: 0;
    margin-bottom: 0;
    font-size: 140%}

.five-cols .col-lg-2, .five-cols .col-md-2, .five-cols .col-sm-2 {
    width: 20%;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
}

.custom-course a:hover {
    text-decoration: none!important;
}
.course-1, .course-2, .course-3, .course-4, .course-5, .course-6, .course-7 {
    width: 100%;
    color: #fff;
    font-size: 230%;
    display: table-cell;
    vertical-align: middle;
    padding: 8px;
    text-align: center;
    box-shadow: 3px 3px 6px #000000;


}

.course-1 {
    background: #e89f1e;
}
.course-2 {
    background: #1697c5;
}
.course-3 {
    background: #71a330;
}
.course-4 {
    background: #27b4ba;
}
.course-5 {
    background: #cf246c;
}
.course-6 {
    background: #df771a;
}
.course-7 {
    background-color: #845f52;
}

.course-1, .course-2, .course-3, .course-4, .course-5, .course-6, .course-7 {
    -webkit-transition: all .3s ease-out;
    -moz-transition: all .3s ease-out;
    -o-transition: all .3s ease-out;
    transition: all .3s ease-out;
    height: 150px;
    -webkit-border-radius: 10px 10px 10px 0;
    -moz-border-radius: 10px;
    -moz-border-radius-bottomleft: 0;
    border-radius: 10px 10px 10px 0;
    position: relative;
    
    
}

.course-1:hover .medical, .course-2:hover .educationnew, .course-3:hover .csecurity, .course-4:hover .software, .course-5:hover .languages, .course-6:hover .cloud-icon, .course-7:hover .vlsi {
    display: none;
}
.course-text {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    visibility: hidden;
    opacity: 0;
    -webkit-transition: visibility opacity .2s;
    text-align:center;
    padding: 5px 0px 5px 0px;
}

.course-1:hover .course-text {
    font-size: 30%!important;
    visibility: visible;
    opacity: 1;
}
.course-1:hover .course-title {
    font-size: 40%!important;
    vertical-align: middle;
    visibility: hidden;
    opacity: 0;
}
.course-2:hover .course-text {
    font-size: 30%!important;
    visibility: visible;
    opacity: 1;
}
.course-2:hover .course-title {
    font-size: 40%!important;
    vertical-align: middle;
    visibility: hidden;
    opacity: 0;
}
.course-3:hover .course-text {
    font-size: 30%!important;
    visibility: visible;
    opacity: 1;
}
.course-3:hover .course-title {
    font-size: 40%!important;
    vertical-align: middle;
    visibility: hidden;
    opacity: 0;
}
.course-4:hover .course-text {
    font-size: 30%!important;
    visibility: visible;
    opacity: 1;
}
.course-4:hover .course-title {
    font-size: 40%!important;
    vertical-align: middle;
    visibility: hidden;
    opacity: 0;
}
.course-5:hover .course-text {
    font-size: 30%!important;
    visibility: visible;
    opacity: 1;
}
.course-5:hover .course-title {
    font-size: 40%!important;
    vertical-align: middle;
    visibility: hidden;
    opacity: 0;
}
.course-6:hover .course-text {
    font-size: 30%!important;
    vertical-align: middle;
    visibility: visible;
    opacity: 1;
    margin: 20px 10px 10px;
}
.course-6:hover .course-title {
    font-size: 40%!important;
    vertical-align: middle;
    visibility: hidden;
    opacity: 0;
}
.course-7:hover .course-text {
    font-size: 30%!important;
    vertical-align: middle;
    visibility: visible;
    opacity: 1;
    margin: 25px 10px 10px;
}
.course-7:hover .course-title {
    font-size: 40%!important;
    vertical-align: middle;
    visibility: hidden;
    opacity: 0;
}

.course-1:hover .course-text, .course-2:hover .course-text, .course-3:hover .course-text, .course-4:hover .course-text, .course-5:hover .course-text {
    margin: 10px 10px 10px;
    vertical-align: middle;
    text-align:center;
}
.course_left{
padding-left:28px;
}

.course-1:hover, .course-2:hover, .course-3:hover, .course-4:hover, .course-5:hover, .course-6:hover, .course-7:hover {
    cursor: pointer;
    -webkit-transform: scale(1.4);
    -moz-transform: scale(1.4);
    -o-transform: scale(1.4);
    -ms-transform: scale(1.4);
    transform: scale(1.4);
    position: relative;
    z-index: 1;
}

.custom-course {
    margin: 10px auto;
}

.course-1, .course-2, .course-3, .course-4, .course-5, .course-6, .course-7 {
    width: 100%;
    color: #fff;
    font-size: 230%;
    display: table-cell;
    vertical-align: middle;
    padding: 5px;
    text-align: center;
}

.course-1 {
    background: #e89f1e;
}
.course-2 {
    background: #1697c5;
}
.course-3 {
    background: #71a330;
}
.course-4 {
    background: #27b4ba;
}
.course-5 {
    background: #cf246c;
}
.course-6 {
    background: #df771a;
}
.course-7 {
    background-color: #845f52;
}

.course-1, .course-2, .course-3, .course-4, .course-5, .course-6, .course-7 {
    -webkit-transition: all .3s ease-out;
    -moz-transition: all .3s ease-out;
    -o-transition: all .3s ease-out;
    transition: all .3s ease-out;
    height: 100px;
    -webkit-border-radius: 10px 10px 10px 0;
    -moz-border-radius: 10px;
    -moz-border-radius-bottomleft: 0;
    border-radius: 10px 10px 10px 0;
    position: relative;
}
.dropdown-menu
{
	 background-color:#ff9347 !important;
	}
	
	.carousel.vertical .carousel-inner {
  height: 100%;
  width: auto;
}
.carousel.vertical .carousel-inner > .item {
  width: auto;
  padding-right: 10px;
  -webkit-transition: 0.6s ease-in-out top;
  transition: 0.6s ease-in-out top;
}
@media all and (transform-3d), (-webkit-transform-3d) {
  .carousel.vertical .carousel-inner > .item {
    -webkit-transition: 0.6s ease-in-out;
    transition: 0.6s ease-in-out;
  }
  .carousel.vertical .carousel-inner > .item.next, .carousel.vertical .carousel-inner > .item.active.right {
    -webkit-transform: translate3d(0, 100%, 0);
            transform: translate3d(0, 100%, 0);
    top: 0;
  }
  .carousel.vertical .carousel-inner > .item.prev, .carousel.vertical .carousel-inner > .item.active.left {
    -webkit-transform: translate3d(0, -100%, 0);
            transform: translate3d(0, -100%, 0);
    top: 0;
  }
  .carousel.vertical .carousel-inner > .item.next.left, .carousel.vertical .carousel-inner > .item.prev.right, .carousel.vertical .carousel-inner > .item.active {
    -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
    top: 0;
  }
}
.carousel.vertical .carousel-inner > .active,
.carousel.vertical .carousel-inner > .next,
.carousel.vertical .carousel-inner > .prev {
  display: block;
}
.carousel.vertical .carousel-inner > .active {
  top: 0;
}
.carousel.vertical .carousel-inner > .next,
.carousel.vertical .carousel-inner > .prev {
  position: absolute;
  top: 0;
  width: 100%;
}
.carousel.vertical .carousel-inner > .next {
  top: 100%;
}
.carousel.vertical .carousel-inner > .prev {
  top: -100%;
}
.carousel.vertical .carousel-inner > .next.left,
.carousel.vertical .carousel-inner > .prev.right {
  top: 0;
}
.carousel.vertical .carousel-inner > .active.left {
  top: -100%;
}
.carousel.vertical .carousel-inner > .active.right {
  top: 100%;
}

.carousel.vertical .carousel-control {
  left: auto;
  width: 50px;
}
.carousel.vertical .carousel-control.up {
  top: 0;
  right: 0;
  bottom: 50%;
}
.carousel.vertical .carousel-control.down {
  top: 50%;
  right: 0;
  bottom: 0;
}
.carousel.vertical .carousel-control .icon-prev,
.carousel.vertical .carousel-control .icon-next,
.carousel.vertical .carousel-control .glyphicon-chevron-up,
.carousel.vertical .carousel-control .glyphicon-chevron-down {
  position: absolute;
  top: 50%;
  z-index: 5;
  display: inline-block;
}
.carousel.vertical .carousel-control .icon-prev,
.carousel.vertical .carousel-control .glyphicon-chevron-up {
  left: 50%;
  margin-left: -10px;
  top: 50%;
  margin-top: -10px;
}
.carousel.vertical .carousel-control .icon-next,
.carousel.vertical .carousel-control .glyphicon-chevron-down {
  left: 50%;
  margin-left: -10px;
  top: 50%;
  margin-top: -10px;
}
.carousel.vertical .carousel-control .icon-up,
.carousel.vertical .carousel-control .icon-down {
  width: 20px;
  height: 20px;
  line-height: 1;
  font-family: serif;
}
.carousel.vertical .carousel-control .icon-prev:before {
  content: '\2039';
}
.carousel.vertical .carousel-control .icon-next:before {
  content: '\203a';
}


.custom-bl-row 
{
	    border-top: 2px solid #ffffff;
    padding-top: 3px;
    margin-top: 1px;
    
}

@media only screen and (max-width: 500px) {
    adjustCube {
       padding-top:20px;
    }
}

 

.custom-info-partt
{
	background-color:#6b6b6b;
	color:#ffffff;
	font-weight:lighter;
	}
	.custom-product-gradientt
	{
		
background-image:url('/index.aspx?id=edu_acts_Strip_1');
	background-repeat:repeat-x;
		 
		 
		}
		.custom-row-marginn
		{
			margin-left:0;
			margin-right:0;
			padding:0 -7;
			
			}
			.separator
			{
				background:url('/index.aspx?id=edu_acts_Strip_3');
				background-position:left;
				background-repeat:no-repeat;
				 
				 
				 
				}
				.submenuseparator
			{
				background:url('/index.aspx?id=edu_acts_Strip_3');
				background-position:left;
				background-repeat:no-repeat;
				 
				 
				 
				}
				.spanstyle
				{
					font-size:12px;
					}
					.spansize11
				{
					font-size:11px;
					}